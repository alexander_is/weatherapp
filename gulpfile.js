var gulp = require("gulp"),
  concat = require("gulp-concat"),
  rename = require("gulp-rename"),
  maps = require("gulp-sourcemaps"),
  del = require("del"),
  plumber = require("gulp-plumber"),
  sequence = require("run-sequence"),
  imagemin = require('gulp-imagemin'),
  browser = require("browser-sync").create();

var DIST = "./build",
  SRC = "./app/";

gulp.task("html", function () {
  return gulp.src(SRC + "**/*.{htm,html}")
  .pipe(gulp.dest(DIST))
  .pipe(browser.reload({stream: true}));
});

gulp.task("images", function () {
  return gulp.src(SRC + "img/**/*.{svg,png,jpeg,jpg,gif,ico}")
  .pipe(imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({plugins: [{removeViewBox: true}]})
  ]))
  .pipe(gulp.dest(DIST + "/img"));
});

gulp.task("css", function () {
  return gulp.src(SRC + "styles/**/*.css")
  .pipe(gulp.dest(DIST + "/styles/"));
});

gulp.task("js", function () {
  return gulp.src(SRC + "script/**/*.js")
  .pipe(plumber())
  .pipe(maps.init())
  .pipe(concat('main.js'))
  .pipe(rename("app.js"))
  .pipe(maps.write())
  .pipe(gulp.dest(DIST + "/js"))
  .pipe(browser.reload({stream: true}));
});

gulp.task("vendor", function () {
  return gulp.src(SRC + "vendor/**/*")
    .pipe(gulp.dest(DIST + "/vendor"));
});

gulp.task("application", ["html", "images", "css", "js", "vendor"]);

gulp.task("clean", function () {
  return del([DIST + "**/*"], {force:true});
});

gulp.task("dist", function (cb) {
  return sequence("clean", ["application"], cb);
});

gulp.task("watch:images", function () {
  return gulp.watch(SRC + "img/**/*.{svg,png,jpeg,jpg,gif,ico}", ["images"]);
});

gulp.task("watch:js", function () {
  return gulp.watch(SRC + "**/*.js", ["js"]);
});

gulp.task("watch:css", function () {
  return gulp.watch(SRC + "styles/**/*.css", ["css"]);
});

gulp.task("watch:html", function () {
  return gulp.watch(SRC + "**/*.{htm,html}", ["html"]);
});

gulp.task("server", function () {
  browser.init({
    server: {
      baseDir: "./build/"
    }
  });
});

gulp.task("watch", ["watch:images", "watch:css", "watch:js", "watch:html", "server"]);
gulp.task("build", ["dist", "server"]);
gulp.task("default", ["watch"]);