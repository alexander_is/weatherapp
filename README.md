# WeatherApp

There are two ways to start the project:

1. for the first one you need to clone the project and run `npm install` after that simply run gulp `build` task that 
will make build folder and automatically runs local server and open project in browser.
2. Alternatively i've included already built folder so that you could simply open *index.html* in your browser.

###About decisions made during develop:

First of all the project structure is divided into folders, each contains same file types. 
Also structural components of an AngularJS is split for the purpose of code readability and maintaining.
There is also vendor folder which contains the minified versions (to reduce loading time) of libs such as AngularJS.

- To prevent Chrome specific CORS error when loading external template if opening without local server or special chrome 
flag that allows such operation i've used inline html inside directive, also left commented path to template

- I've decided not to use ready libraries for styles such as Bootstrap or Foundation because of reducing unnecessary 
amount of code for small project

- API request are grouped in service for single responsibility

- I've also used timeouts to show implemented loaders as requests are send to fast 

