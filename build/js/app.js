angular.module('weatherApp', ['ngAnimate']);
(function () {
  'use strict';

  angular
    .module('weatherApp')
    .controller('weatherCtrl', weatherCtrl);

  weatherCtrl.$inject = ['$interval', 'weatherService'];

  function weatherCtrl($interval, weatherService) {
    var vm = this;
    vm.degreeType = 'C';
    vm.metricSystem = true;
    vm.switchTempType = switchTemp;
    vm.cities = weatherService.cities;

    function switchTemp(type) {
      if (type === 'C') {
        vm.degreeType = 'C';
        vm.metricSystem = true;
      } else if (type === 'F') {
        vm.degreeType = 'F';
        vm.metricSystem = false;
      }
    }

    var tick = function() {
      vm.clock = Date.now();
    };
    tick();
    $interval(tick, 1000);
  }
})();
/* I've used inline template to have possibility of opening the index.html directly in browser.
   In other case Chrome will throw a CORS error.
*/
(function () {
  'use strict';

  angular
  .module('weatherApp')
  .directive('cityWeather', cityWeather);

  cityWeather.$inject = [];

  function cityWeather() {
    return {
      restrict: 'EA',
      scope: {
        cityName: '@',
        degreeType: '@'
      },
      //templateUrl: 'templates/city-weather.html'
      template: '<div class="cities">\n' +
      '  <div class="primary" ng-click="vm.showMore()">\n' +
      '    <div class="cities__name">\n' +
      '      {{cityName}}\n' +
      '    </div>\n' +
      '    <div class="cities__weather-icon" ng-if="!vm.loading">\n' +
      '      <img ng-src="{{vm.iconSrc}}" alt="weather icon">\n' +
      '    </div>\n' +
      '    <div class="cities__weather-info" ng-if="!vm.loading">\n' +
      '      <div class="average-temp">\n' +
      '        <span class="temp">{{vm.temp}}</span> {{vm.degreeUnits}}&deg;\n' +
      '      </div>\n' +
      '      <div class="wind-speed">\n' +
      '        <div class="wind-icon">\n' +
      '          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M21.688 6.668c.172 3.71-3.595 6.901-6.56 9.735-2.175 2.079-3.849 4.743-4.03 7.597-2.059-1.342-2.509-3.797-2.123-5.938.53-2.95-2.231-4.9-3.293-5.822-2.15-1.867-3.682-4.216-3.682-6.647 0-4.607 5.533-5.593 10.108-5.593 3.397 0 9.892.913 9.892 3.798 0 1.979-5.022 3.202-9.56 3.202-8.735 0-7.32-3.886-1.765-3.886.9 0 1.821.097 2.622.323-2.927.057-6.493 1.563-1.189 1.563 1.136 0 7.163-.385 7.163-1.5s-5.303-1.578-7.163-1.578c-3.281 0-8.108.609-8.108 3.671 0 1.636 1.063 3.46 2.994 5.137l.311.262c1.29 1.076 4.307 3.591 3.646 7.389.72-1.172 1.851-2.511 2.946-3.568-.542 0-1.602-.302-2.229-.75 1.071.089 3.084-.188 4.604-1.562-1.312 0-2.787-.219-3.74-1.062 2.44.155 5.303-.312 6.282-1.854-1.625.333-4.834.104-6.702-.875 3.045.045 7.022-.462 9.578-2.079l-.001.028.003-.001-.004.01zm-7.688 15.332c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm-7 0c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm-3-1c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm2-2c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm10 0c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm2 2c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1z"/></svg>\n' +
      '        </div>\n' +
      '        <div class="speed">\n' +
      '          {{vm.windSpeed}}\n' +
      '        </div>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '    <div ng-if="vm.loading">\n' +
      '      <div class="single13"></div>\n' +
      '    </div>\n' +
      '  </div>\n' +
      '  <div class="more-info" ng-show="vm.expanded">\n' +
      '    <div class="forecast" ng-repeat="item in vm.hoursData track by $index">\n' +
      '      <div class="forecast__time">\n' +
      '        {{vm.convertTime(item.dt) | date: \'HH:mm a\'}}\n' +
      '      </div>\n' +
      '      <div class="forecast__temp">\n' +
      '        Min: <span class="max">{{vm.round(item.main.temp_max, 1)}} {{vm.degreeUnits}}&deg;</span>\n' +
      '        Max: <span class="max">{{vm.round(item.main.temp_min, 1)}} {{vm.degreeUnits}}&deg;</span>\n' +
      '      </div>\n' +
      '      <div class="forecast__wind">\n' +
      '        <div class="wind-speed">\n' +
      '          <div class="wind-icon">\n' +
      '            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M21.688 6.668c.172 3.71-3.595 6.901-6.56 9.735-2.175 2.079-3.849 4.743-4.03 7.597-2.059-1.342-2.509-3.797-2.123-5.938.53-2.95-2.231-4.9-3.293-5.822-2.15-1.867-3.682-4.216-3.682-6.647 0-4.607 5.533-5.593 10.108-5.593 3.397 0 9.892.913 9.892 3.798 0 1.979-5.022 3.202-9.56 3.202-8.735 0-7.32-3.886-1.765-3.886.9 0 1.821.097 2.622.323-2.927.057-6.493 1.563-1.189 1.563 1.136 0 7.163-.385 7.163-1.5s-5.303-1.578-7.163-1.578c-3.281 0-8.108.609-8.108 3.671 0 1.636 1.063 3.46 2.994 5.137l.311.262c1.29 1.076 4.307 3.591 3.646 7.389.72-1.172 1.851-2.511 2.946-3.568-.542 0-1.602-.302-2.229-.75 1.071.089 3.084-.188 4.604-1.562-1.312 0-2.787-.219-3.74-1.062 2.44.155 5.303-.312 6.282-1.854-1.625.333-4.834.104-6.702-.875 3.045.045 7.022-.462 9.578-2.079l-.001.028.003-.001-.004.01zm-7.688 15.332c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm-7 0c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm-3-1c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm2-2c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm10 0c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1zm2 2c-.553 0-1 .448-1 1s.447 1 1 1 1-.448 1-1-.447-1-1-1z"/></svg>\n' +
      '          </div>\n' +
      '          <div class="speed">\n' +
      '            {{item.wind.speed}}\n' +
      '          </div>\n' +
      '        </div>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '  </div>\n' +
      '</div>',
      controller: ['weatherService', '$scope', '$timeout', function (weatherService, $scope, $timeout) {
        var vm = this,
            city = $scope.cityName,
            metric = 'metric',
            imperic = 'imperial';

        vm.temp = 0;
        vm.windSpeed = 0;
        vm.expanded = false;
        vm.units = metric;
        vm.showMore = showMore;
        vm.convertTime = convertTime;
        vm.round = roundTemp;

        $scope.$watch('degreeType', function (newVal, oldVal) {
          if (!angular.equals(newVal, oldVal)) {
            vm.units = $scope.degreeType === 'C' ? metric : imperic;
            vm.expanded = false;
            $timeout(function () {
              getWeather();
              getWeatherForecast();
            }, 500);
          }
        });

        function showMore() {
          if (!vm.expanded) {
            vm.expanded = !vm.expanded;
            getWeatherForecast();
          } else {
            vm.expanded = !vm.expanded;
          }
        }

        function getWeatherForecast() {
          weatherService.getForecast(city, vm.units)
          .then(function (res) {
            if (res.status === 200) {
              vm.hoursData = res.data.list.slice(0, 3);
              vm.degreeUnits = $scope.degreeType;
            } else {
              console.log(res);
              throw new Error ('Response status is bad');
            }
          }, function (err) {
            console.log(err);
          })
        }

        function getWeather () {
          vm.loading = true;
          weatherService.getWeather(city, vm.units)
          .then(function (res) {
            if (res.status === 200) {
              vm.temp = roundTemp(res.data.main.temp, 1);
              vm.windSpeed = res.data.wind.speed;
              vm.iconSrc = "http://openweathermap.org/img/w/" + res.data.weather[0].icon + ".png";
              vm.degreeUnits = $scope.degreeType;
            } else {
              console.log(res);
              throw new Error ('Response status is bad');
            }
            $timeout(function () {
              vm.loading = false;
            }, 1000);
          }, function (err) {
            console.log(err);
          });
        }
        getWeather();

        function convertTime(time) {
          return new Date(time * 1000);
        }

        function roundTemp(temp, to) {
          return (Math.round(temp * 100)/100).toFixed(to);
        }

      }],
      controllerAs: 'vm'
    }
  }
})();
(function () {
  'use strict';

  angular
  .module('weatherApp')
  .service('weatherService', weatherService);

  weatherService.$inject = ['$http'];

  function weatherService($http) {
    var obj = {
      cities: ['Kyiv', 'Amsterdam', 'London', 'Paris', 'Dublin'],
      getWeather: getWeather,
      getForecast: getForecast
    };

    function getWeather(city, units) {
      return $http.get('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=' + units + '&appid=3d8b309701a13f65b660fa2c64cdc517');
    }

    function getForecast(city, units) {
      return $http.get('http://api.openweathermap.org/data/2.5/forecast?q=' + city + '&units=' + units + '&appid=3d8b309701a13f65b660fa2c64cdc517');
    }

    return obj;
  }
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsIndlYXRoZXIuY29udHJvbGxlci5qcyIsIndlYXRoZXIuZGlyZWN0aXZlLmpzIiwid2VhdGhlci5zZXJ2aWNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdEpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJhbmd1bGFyLm1vZHVsZSgnd2VhdGhlckFwcCcsIFsnbmdBbmltYXRlJ10pOyIsIihmdW5jdGlvbiAoKSB7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBhbmd1bGFyXHJcbiAgICAubW9kdWxlKCd3ZWF0aGVyQXBwJylcclxuICAgIC5jb250cm9sbGVyKCd3ZWF0aGVyQ3RybCcsIHdlYXRoZXJDdHJsKTtcclxuXHJcbiAgd2VhdGhlckN0cmwuJGluamVjdCA9IFsnJGludGVydmFsJywgJ3dlYXRoZXJTZXJ2aWNlJ107XHJcblxyXG4gIGZ1bmN0aW9uIHdlYXRoZXJDdHJsKCRpbnRlcnZhbCwgd2VhdGhlclNlcnZpY2UpIHtcclxuICAgIHZhciB2bSA9IHRoaXM7XHJcbiAgICB2bS5kZWdyZWVUeXBlID0gJ0MnO1xyXG4gICAgdm0ubWV0cmljU3lzdGVtID0gdHJ1ZTtcclxuICAgIHZtLnN3aXRjaFRlbXBUeXBlID0gc3dpdGNoVGVtcDtcclxuICAgIHZtLmNpdGllcyA9IHdlYXRoZXJTZXJ2aWNlLmNpdGllcztcclxuXHJcbiAgICBmdW5jdGlvbiBzd2l0Y2hUZW1wKHR5cGUpIHtcclxuICAgICAgaWYgKHR5cGUgPT09ICdDJykge1xyXG4gICAgICAgIHZtLmRlZ3JlZVR5cGUgPSAnQyc7XHJcbiAgICAgICAgdm0ubWV0cmljU3lzdGVtID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIGlmICh0eXBlID09PSAnRicpIHtcclxuICAgICAgICB2bS5kZWdyZWVUeXBlID0gJ0YnO1xyXG4gICAgICAgIHZtLm1ldHJpY1N5c3RlbSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIHRpY2sgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdm0uY2xvY2sgPSBEYXRlLm5vdygpO1xyXG4gICAgfTtcclxuICAgIHRpY2soKTtcclxuICAgICRpbnRlcnZhbCh0aWNrLCAxMDAwKTtcclxuICB9XHJcbn0pKCk7IiwiLyogSSd2ZSB1c2VkIGlubGluZSB0ZW1wbGF0ZSB0byBoYXZlIHBvc3NpYmlsaXR5IG9mIG9wZW5pbmcgdGhlIGluZGV4Lmh0bWwgZGlyZWN0bHkgaW4gYnJvd3Nlci5cclxuICAgSW4gb3RoZXIgY2FzZSBDaHJvbWUgd2lsbCB0aHJvdyBhIENPUlMgZXJyb3IuXHJcbiovXHJcbihmdW5jdGlvbiAoKSB7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBhbmd1bGFyXHJcbiAgLm1vZHVsZSgnd2VhdGhlckFwcCcpXHJcbiAgLmRpcmVjdGl2ZSgnY2l0eVdlYXRoZXInLCBjaXR5V2VhdGhlcik7XHJcblxyXG4gIGNpdHlXZWF0aGVyLiRpbmplY3QgPSBbXTtcclxuXHJcbiAgZnVuY3Rpb24gY2l0eVdlYXRoZXIoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICByZXN0cmljdDogJ0VBJyxcclxuICAgICAgc2NvcGU6IHtcclxuICAgICAgICBjaXR5TmFtZTogJ0AnLFxyXG4gICAgICAgIGRlZ3JlZVR5cGU6ICdAJ1xyXG4gICAgICB9LFxyXG4gICAgICAvL3RlbXBsYXRlVXJsOiAndGVtcGxhdGVzL2NpdHktd2VhdGhlci5odG1sJ1xyXG4gICAgICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJjaXRpZXNcIj5cXG4nICtcclxuICAgICAgJyAgPGRpdiBjbGFzcz1cInByaW1hcnlcIiBuZy1jbGljaz1cInZtLnNob3dNb3JlKClcIj5cXG4nICtcclxuICAgICAgJyAgICA8ZGl2IGNsYXNzPVwiY2l0aWVzX19uYW1lXCI+XFxuJyArXHJcbiAgICAgICcgICAgICB7e2NpdHlOYW1lfX1cXG4nICtcclxuICAgICAgJyAgICA8L2Rpdj5cXG4nICtcclxuICAgICAgJyAgICA8ZGl2IGNsYXNzPVwiY2l0aWVzX193ZWF0aGVyLWljb25cIiBuZy1pZj1cIiF2bS5sb2FkaW5nXCI+XFxuJyArXHJcbiAgICAgICcgICAgICA8aW1nIG5nLXNyYz1cInt7dm0uaWNvblNyY319XCIgYWx0PVwid2VhdGhlciBpY29uXCI+XFxuJyArXHJcbiAgICAgICcgICAgPC9kaXY+XFxuJyArXHJcbiAgICAgICcgICAgPGRpdiBjbGFzcz1cImNpdGllc19fd2VhdGhlci1pbmZvXCIgbmctaWY9XCIhdm0ubG9hZGluZ1wiPlxcbicgK1xyXG4gICAgICAnICAgICAgPGRpdiBjbGFzcz1cImF2ZXJhZ2UtdGVtcFwiPlxcbicgK1xyXG4gICAgICAnICAgICAgICA8c3BhbiBjbGFzcz1cInRlbXBcIj57e3ZtLnRlbXB9fTwvc3Bhbj4ge3t2bS5kZWdyZWVVbml0c319JmRlZztcXG4nICtcclxuICAgICAgJyAgICAgIDwvZGl2PlxcbicgK1xyXG4gICAgICAnICAgICAgPGRpdiBjbGFzcz1cIndpbmQtc3BlZWRcIj5cXG4nICtcclxuICAgICAgJyAgICAgICAgPGRpdiBjbGFzcz1cIndpbmQtaWNvblwiPlxcbicgK1xyXG4gICAgICAnICAgICAgICAgIDxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHdpZHRoPVwiMjRcIiBoZWlnaHQ9XCIyNFwiIHZpZXdCb3g9XCIwIDAgMjQgMjRcIj48cGF0aCBkPVwiTTIxLjY4OCA2LjY2OGMuMTcyIDMuNzEtMy41OTUgNi45MDEtNi41NiA5LjczNS0yLjE3NSAyLjA3OS0zLjg0OSA0Ljc0My00LjAzIDcuNTk3LTIuMDU5LTEuMzQyLTIuNTA5LTMuNzk3LTIuMTIzLTUuOTM4LjUzLTIuOTUtMi4yMzEtNC45LTMuMjkzLTUuODIyLTIuMTUtMS44NjctMy42ODItNC4yMTYtMy42ODItNi42NDcgMC00LjYwNyA1LjUzMy01LjU5MyAxMC4xMDgtNS41OTMgMy4zOTcgMCA5Ljg5Mi45MTMgOS44OTIgMy43OTggMCAxLjk3OS01LjAyMiAzLjIwMi05LjU2IDMuMjAyLTguNzM1IDAtNy4zMi0zLjg4Ni0xLjc2NS0zLjg4Ni45IDAgMS44MjEuMDk3IDIuNjIyLjMyMy0yLjkyNy4wNTctNi40OTMgMS41NjMtMS4xODkgMS41NjMgMS4xMzYgMCA3LjE2My0uMzg1IDcuMTYzLTEuNXMtNS4zMDMtMS41NzgtNy4xNjMtMS41NzhjLTMuMjgxIDAtOC4xMDguNjA5LTguMTA4IDMuNjcxIDAgMS42MzYgMS4wNjMgMy40NiAyLjk5NCA1LjEzN2wuMzExLjI2MmMxLjI5IDEuMDc2IDQuMzA3IDMuNTkxIDMuNjQ2IDcuMzg5LjcyLTEuMTcyIDEuODUxLTIuNTExIDIuOTQ2LTMuNTY4LS41NDIgMC0xLjYwMi0uMzAyLTIuMjI5LS43NSAxLjA3MS4wODkgMy4wODQtLjE4OCA0LjYwNC0xLjU2Mi0xLjMxMiAwLTIuNzg3LS4yMTktMy43NC0xLjA2MiAyLjQ0LjE1NSA1LjMwMy0uMzEyIDYuMjgyLTEuODU0LTEuNjI1LjMzMy00LjgzNC4xMDQtNi43MDItLjg3NSAzLjA0NS4wNDUgNy4wMjItLjQ2MiA5LjU3OC0yLjA3OWwtLjAwMS4wMjguMDAzLS4wMDEtLjAwNC4wMXptLTcuNjg4IDE1LjMzMmMtLjU1MyAwLTEgLjQ0OC0xIDFzLjQ0NyAxIDEgMSAxLS40NDggMS0xLS40NDctMS0xLTF6bS03IDBjLS41NTMgMC0xIC40NDgtMSAxcy40NDcgMSAxIDEgMS0uNDQ4IDEtMS0uNDQ3LTEtMS0xem0tMy0xYy0uNTUzIDAtMSAuNDQ4LTEgMXMuNDQ3IDEgMSAxIDEtLjQ0OCAxLTEtLjQ0Ny0xLTEtMXptMi0yYy0uNTUzIDAtMSAuNDQ4LTEgMXMuNDQ3IDEgMSAxIDEtLjQ0OCAxLTEtLjQ0Ny0xLTEtMXptMTAgMGMtLjU1MyAwLTEgLjQ0OC0xIDFzLjQ0NyAxIDEgMSAxLS40NDggMS0xLS40NDctMS0xLTF6bTIgMmMtLjU1MyAwLTEgLjQ0OC0xIDFzLjQ0NyAxIDEgMSAxLS40NDggMS0xLS40NDctMS0xLTF6XCIvPjwvc3ZnPlxcbicgK1xyXG4gICAgICAnICAgICAgICA8L2Rpdj5cXG4nICtcclxuICAgICAgJyAgICAgICAgPGRpdiBjbGFzcz1cInNwZWVkXCI+XFxuJyArXHJcbiAgICAgICcgICAgICAgICAge3t2bS53aW5kU3BlZWR9fVxcbicgK1xyXG4gICAgICAnICAgICAgICA8L2Rpdj5cXG4nICtcclxuICAgICAgJyAgICAgIDwvZGl2PlxcbicgK1xyXG4gICAgICAnICAgIDwvZGl2PlxcbicgK1xyXG4gICAgICAnICAgIDxkaXYgbmctaWY9XCJ2bS5sb2FkaW5nXCI+XFxuJyArXHJcbiAgICAgICcgICAgICA8ZGl2IGNsYXNzPVwic2luZ2xlMTNcIj48L2Rpdj5cXG4nICtcclxuICAgICAgJyAgICA8L2Rpdj5cXG4nICtcclxuICAgICAgJyAgPC9kaXY+XFxuJyArXHJcbiAgICAgICcgIDxkaXYgY2xhc3M9XCJtb3JlLWluZm9cIiBuZy1zaG93PVwidm0uZXhwYW5kZWRcIj5cXG4nICtcclxuICAgICAgJyAgICA8ZGl2IGNsYXNzPVwiZm9yZWNhc3RcIiBuZy1yZXBlYXQ9XCJpdGVtIGluIHZtLmhvdXJzRGF0YSB0cmFjayBieSAkaW5kZXhcIj5cXG4nICtcclxuICAgICAgJyAgICAgIDxkaXYgY2xhc3M9XCJmb3JlY2FzdF9fdGltZVwiPlxcbicgK1xyXG4gICAgICAnICAgICAgICB7e3ZtLmNvbnZlcnRUaW1lKGl0ZW0uZHQpIHwgZGF0ZTogXFwnSEg6bW0gYVxcJ319XFxuJyArXHJcbiAgICAgICcgICAgICA8L2Rpdj5cXG4nICtcclxuICAgICAgJyAgICAgIDxkaXYgY2xhc3M9XCJmb3JlY2FzdF9fdGVtcFwiPlxcbicgK1xyXG4gICAgICAnICAgICAgICBNaW46IDxzcGFuIGNsYXNzPVwibWF4XCI+e3t2bS5yb3VuZChpdGVtLm1haW4udGVtcF9tYXgsIDEpfX0ge3t2bS5kZWdyZWVVbml0c319JmRlZzs8L3NwYW4+XFxuJyArXHJcbiAgICAgICcgICAgICAgIE1heDogPHNwYW4gY2xhc3M9XCJtYXhcIj57e3ZtLnJvdW5kKGl0ZW0ubWFpbi50ZW1wX21pbiwgMSl9fSB7e3ZtLmRlZ3JlZVVuaXRzfX0mZGVnOzwvc3Bhbj5cXG4nICtcclxuICAgICAgJyAgICAgIDwvZGl2PlxcbicgK1xyXG4gICAgICAnICAgICAgPGRpdiBjbGFzcz1cImZvcmVjYXN0X193aW5kXCI+XFxuJyArXHJcbiAgICAgICcgICAgICAgIDxkaXYgY2xhc3M9XCJ3aW5kLXNwZWVkXCI+XFxuJyArXHJcbiAgICAgICcgICAgICAgICAgPGRpdiBjbGFzcz1cIndpbmQtaWNvblwiPlxcbicgK1xyXG4gICAgICAnICAgICAgICAgICAgPHN2ZyB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIgd2lkdGg9XCIyNFwiIGhlaWdodD1cIjI0XCIgdmlld0JveD1cIjAgMCAyNCAyNFwiPjxwYXRoIGQ9XCJNMjEuNjg4IDYuNjY4Yy4xNzIgMy43MS0zLjU5NSA2LjkwMS02LjU2IDkuNzM1LTIuMTc1IDIuMDc5LTMuODQ5IDQuNzQzLTQuMDMgNy41OTctMi4wNTktMS4zNDItMi41MDktMy43OTctMi4xMjMtNS45MzguNTMtMi45NS0yLjIzMS00LjktMy4yOTMtNS44MjItMi4xNS0xLjg2Ny0zLjY4Mi00LjIxNi0zLjY4Mi02LjY0NyAwLTQuNjA3IDUuNTMzLTUuNTkzIDEwLjEwOC01LjU5MyAzLjM5NyAwIDkuODkyLjkxMyA5Ljg5MiAzLjc5OCAwIDEuOTc5LTUuMDIyIDMuMjAyLTkuNTYgMy4yMDItOC43MzUgMC03LjMyLTMuODg2LTEuNzY1LTMuODg2LjkgMCAxLjgyMS4wOTcgMi42MjIuMzIzLTIuOTI3LjA1Ny02LjQ5MyAxLjU2My0xLjE4OSAxLjU2MyAxLjEzNiAwIDcuMTYzLS4zODUgNy4xNjMtMS41cy01LjMwMy0xLjU3OC03LjE2My0xLjU3OGMtMy4yODEgMC04LjEwOC42MDktOC4xMDggMy42NzEgMCAxLjYzNiAxLjA2MyAzLjQ2IDIuOTk0IDUuMTM3bC4zMTEuMjYyYzEuMjkgMS4wNzYgNC4zMDcgMy41OTEgMy42NDYgNy4zODkuNzItMS4xNzIgMS44NTEtMi41MTEgMi45NDYtMy41NjgtLjU0MiAwLTEuNjAyLS4zMDItMi4yMjktLjc1IDEuMDcxLjA4OSAzLjA4NC0uMTg4IDQuNjA0LTEuNTYyLTEuMzEyIDAtMi43ODctLjIxOS0zLjc0LTEuMDYyIDIuNDQuMTU1IDUuMzAzLS4zMTIgNi4yODItMS44NTQtMS42MjUuMzMzLTQuODM0LjEwNC02LjcwMi0uODc1IDMuMDQ1LjA0NSA3LjAyMi0uNDYyIDkuNTc4LTIuMDc5bC0uMDAxLjAyOC4wMDMtLjAwMS0uMDA0LjAxem0tNy42ODggMTUuMzMyYy0uNTUzIDAtMSAuNDQ4LTEgMXMuNDQ3IDEgMSAxIDEtLjQ0OCAxLTEtLjQ0Ny0xLTEtMXptLTcgMGMtLjU1MyAwLTEgLjQ0OC0xIDFzLjQ0NyAxIDEgMSAxLS40NDggMS0xLS40NDctMS0xLTF6bS0zLTFjLS41NTMgMC0xIC40NDgtMSAxcy40NDcgMSAxIDEgMS0uNDQ4IDEtMS0uNDQ3LTEtMS0xem0yLTJjLS41NTMgMC0xIC40NDgtMSAxcy40NDcgMSAxIDEgMS0uNDQ4IDEtMS0uNDQ3LTEtMS0xem0xMCAwYy0uNTUzIDAtMSAuNDQ4LTEgMXMuNDQ3IDEgMSAxIDEtLjQ0OCAxLTEtLjQ0Ny0xLTEtMXptMiAyYy0uNTUzIDAtMSAuNDQ4LTEgMXMuNDQ3IDEgMSAxIDEtLjQ0OCAxLTEtLjQ0Ny0xLTEtMXpcIi8+PC9zdmc+XFxuJyArXHJcbiAgICAgICcgICAgICAgICAgPC9kaXY+XFxuJyArXHJcbiAgICAgICcgICAgICAgICAgPGRpdiBjbGFzcz1cInNwZWVkXCI+XFxuJyArXHJcbiAgICAgICcgICAgICAgICAgICB7e2l0ZW0ud2luZC5zcGVlZH19XFxuJyArXHJcbiAgICAgICcgICAgICAgICAgPC9kaXY+XFxuJyArXHJcbiAgICAgICcgICAgICAgIDwvZGl2PlxcbicgK1xyXG4gICAgICAnICAgICAgPC9kaXY+XFxuJyArXHJcbiAgICAgICcgICAgPC9kaXY+XFxuJyArXHJcbiAgICAgICcgIDwvZGl2PlxcbicgK1xyXG4gICAgICAnPC9kaXY+JyxcclxuICAgICAgY29udHJvbGxlcjogWyd3ZWF0aGVyU2VydmljZScsICckc2NvcGUnLCAnJHRpbWVvdXQnLCBmdW5jdGlvbiAod2VhdGhlclNlcnZpY2UsICRzY29wZSwgJHRpbWVvdXQpIHtcclxuICAgICAgICB2YXIgdm0gPSB0aGlzLFxyXG4gICAgICAgICAgICBjaXR5ID0gJHNjb3BlLmNpdHlOYW1lLFxyXG4gICAgICAgICAgICBtZXRyaWMgPSAnbWV0cmljJyxcclxuICAgICAgICAgICAgaW1wZXJpYyA9ICdpbXBlcmlhbCc7XHJcblxyXG4gICAgICAgIHZtLnRlbXAgPSAwO1xyXG4gICAgICAgIHZtLndpbmRTcGVlZCA9IDA7XHJcbiAgICAgICAgdm0uZXhwYW5kZWQgPSBmYWxzZTtcclxuICAgICAgICB2bS51bml0cyA9IG1ldHJpYztcclxuICAgICAgICB2bS5zaG93TW9yZSA9IHNob3dNb3JlO1xyXG4gICAgICAgIHZtLmNvbnZlcnRUaW1lID0gY29udmVydFRpbWU7XHJcbiAgICAgICAgdm0ucm91bmQgPSByb3VuZFRlbXA7XHJcblxyXG4gICAgICAgICRzY29wZS4kd2F0Y2goJ2RlZ3JlZVR5cGUnLCBmdW5jdGlvbiAobmV3VmFsLCBvbGRWYWwpIHtcclxuICAgICAgICAgIGlmICghYW5ndWxhci5lcXVhbHMobmV3VmFsLCBvbGRWYWwpKSB7XHJcbiAgICAgICAgICAgIHZtLnVuaXRzID0gJHNjb3BlLmRlZ3JlZVR5cGUgPT09ICdDJyA/IG1ldHJpYyA6IGltcGVyaWM7XHJcbiAgICAgICAgICAgIHZtLmV4cGFuZGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICBnZXRXZWF0aGVyKCk7XHJcbiAgICAgICAgICAgICAgZ2V0V2VhdGhlckZvcmVjYXN0KCk7XHJcbiAgICAgICAgICAgIH0sIDUwMCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIHNob3dNb3JlKCkge1xyXG4gICAgICAgICAgaWYgKCF2bS5leHBhbmRlZCkge1xyXG4gICAgICAgICAgICB2bS5leHBhbmRlZCA9ICF2bS5leHBhbmRlZDtcclxuICAgICAgICAgICAgZ2V0V2VhdGhlckZvcmVjYXN0KCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB2bS5leHBhbmRlZCA9ICF2bS5leHBhbmRlZDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGdldFdlYXRoZXJGb3JlY2FzdCgpIHtcclxuICAgICAgICAgIHdlYXRoZXJTZXJ2aWNlLmdldEZvcmVjYXN0KGNpdHksIHZtLnVuaXRzKVxyXG4gICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHJlcykge1xyXG4gICAgICAgICAgICBpZiAocmVzLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgdm0uaG91cnNEYXRhID0gcmVzLmRhdGEubGlzdC5zbGljZSgwLCAzKTtcclxuICAgICAgICAgICAgICB2bS5kZWdyZWVVbml0cyA9ICRzY29wZS5kZWdyZWVUeXBlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcyk7XHJcbiAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yICgnUmVzcG9uc2Ugc3RhdHVzIGlzIGJhZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0V2VhdGhlciAoKSB7XHJcbiAgICAgICAgICB2bS5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgIHdlYXRoZXJTZXJ2aWNlLmdldFdlYXRoZXIoY2l0eSwgdm0udW5pdHMpXHJcbiAgICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzKSB7XHJcbiAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzID09PSAyMDApIHtcclxuICAgICAgICAgICAgICB2bS50ZW1wID0gcm91bmRUZW1wKHJlcy5kYXRhLm1haW4udGVtcCwgMSk7XHJcbiAgICAgICAgICAgICAgdm0ud2luZFNwZWVkID0gcmVzLmRhdGEud2luZC5zcGVlZDtcclxuICAgICAgICAgICAgICB2bS5pY29uU3JjID0gXCJodHRwOi8vb3BlbndlYXRoZXJtYXAub3JnL2ltZy93L1wiICsgcmVzLmRhdGEud2VhdGhlclswXS5pY29uICsgXCIucG5nXCI7XHJcbiAgICAgICAgICAgICAgdm0uZGVncmVlVW5pdHMgPSAkc2NvcGUuZGVncmVlVHlwZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xyXG4gICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvciAoJ1Jlc3BvbnNlIHN0YXR1cyBpcyBiYWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAkdGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgdm0ubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9LCAxMDAwKTtcclxuICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnIpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coZXJyKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBnZXRXZWF0aGVyKCk7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGNvbnZlcnRUaW1lKHRpbWUpIHtcclxuICAgICAgICAgIHJldHVybiBuZXcgRGF0ZSh0aW1lICogMTAwMCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiByb3VuZFRlbXAodGVtcCwgdG8pIHtcclxuICAgICAgICAgIHJldHVybiAoTWF0aC5yb3VuZCh0ZW1wICogMTAwKS8xMDApLnRvRml4ZWQodG8pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIH1dLFxyXG4gICAgICBjb250cm9sbGVyQXM6ICd2bSdcclxuICAgIH1cclxuICB9XHJcbn0pKCk7IiwiKGZ1bmN0aW9uICgpIHtcclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAubW9kdWxlKCd3ZWF0aGVyQXBwJylcclxuICAuc2VydmljZSgnd2VhdGhlclNlcnZpY2UnLCB3ZWF0aGVyU2VydmljZSk7XHJcblxyXG4gIHdlYXRoZXJTZXJ2aWNlLiRpbmplY3QgPSBbJyRodHRwJ107XHJcblxyXG4gIGZ1bmN0aW9uIHdlYXRoZXJTZXJ2aWNlKCRodHRwKSB7XHJcbiAgICB2YXIgb2JqID0ge1xyXG4gICAgICBjaXRpZXM6IFsnS3lpdicsICdBbXN0ZXJkYW0nLCAnTG9uZG9uJywgJ1BhcmlzJywgJ0R1YmxpbiddLFxyXG4gICAgICBnZXRXZWF0aGVyOiBnZXRXZWF0aGVyLFxyXG4gICAgICBnZXRGb3JlY2FzdDogZ2V0Rm9yZWNhc3RcclxuICAgIH07XHJcblxyXG4gICAgZnVuY3Rpb24gZ2V0V2VhdGhlcihjaXR5LCB1bml0cykge1xyXG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCdodHRwOi8vYXBpLm9wZW53ZWF0aGVybWFwLm9yZy9kYXRhLzIuNS93ZWF0aGVyP3E9JyArIGNpdHkgKyAnJnVuaXRzPScgKyB1bml0cyArICcmYXBwaWQ9M2Q4YjMwOTcwMWExM2Y2NWI2NjBmYTJjNjRjZGM1MTcnKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBnZXRGb3JlY2FzdChjaXR5LCB1bml0cykge1xyXG4gICAgICByZXR1cm4gJGh0dHAuZ2V0KCdodHRwOi8vYXBpLm9wZW53ZWF0aGVybWFwLm9yZy9kYXRhLzIuNS9mb3JlY2FzdD9xPScgKyBjaXR5ICsgJyZ1bml0cz0nICsgdW5pdHMgKyAnJmFwcGlkPTNkOGIzMDk3MDFhMTNmNjViNjYwZmEyYzY0Y2RjNTE3Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG9iajtcclxuICB9XHJcbn0pKCk7Il19
