(function () {
  'use strict';

  angular
    .module('weatherApp')
    .controller('weatherCtrl', weatherCtrl);

  weatherCtrl.$inject = ['$interval', 'weatherService'];

  function weatherCtrl($interval, weatherService) {
    var vm = this;
    vm.degreeType = 'C';
    vm.metricSystem = true;
    vm.switchTempType = switchTemp;
    vm.cities = weatherService.cities;

    function switchTemp(type) {
      if (type === 'C') {
        vm.degreeType = 'C';
        vm.metricSystem = true;
      } else if (type === 'F') {
        vm.degreeType = 'F';
        vm.metricSystem = false;
      }
    }

    var tick = function() {
      vm.clock = Date.now();
    };
    tick();
    $interval(tick, 1000);
  }
})();