(function () {
  'use strict';

  angular
  .module('weatherApp')
  .service('weatherService', weatherService);

  weatherService.$inject = ['$http'];

  function weatherService($http) {
    var obj = {
      cities: ['Kyiv', 'Amsterdam', 'London', 'Paris', 'Dublin'],
      getWeather: getWeather,
      getForecast: getForecast
    };

    function getWeather(city, units) {
      return $http.get('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=' + units + '&appid=3d8b309701a13f65b660fa2c64cdc517');
    }

    function getForecast(city, units) {
      return $http.get('http://api.openweathermap.org/data/2.5/forecast?q=' + city + '&units=' + units + '&appid=3d8b309701a13f65b660fa2c64cdc517');
    }

    return obj;
  }
})();